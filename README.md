# logo

The group's logo.

## SVG naming convention:
text_background_blue_line.svg
* text: 
    * text = includes text
    * ntext = no text included
* background:
    * bg = includes ox blue background
    * nbg = includes transparent background
* blue:
    * db = uses a dark blue on the blue LED
    * lb = uses lighr blue on the blue led
* line:
    * dl = uses dark line for perovskite part
    * ll = uses light line for perovskite part